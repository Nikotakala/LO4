#!/bin/bash
git init
echo "node_modules" >> .gitignore
echo ".DS_Store" >> .gitignore
npm init -y
npm install --save express
npm install --save-dev mocha chai
# set ECMAScript to package.json
npm pkg set 'type'='module'
npm pkg set 'scripts.dev'='node --watch-path=src src/main.js'
npm pkg set 'scripts.start'='node src/main.js'
npm pkg set 'scripts.test'='mocha'
# project structure
mkdir src test
touch src/main.js
touch src/converter.js # unit
touch src/routes.js # integrates converter
touch test/converter.spec.js # unit tests only
touch test/routes.spec.js # integration tests + possible unit tests


# GIT
git remote add origin https://gitlab.com/nikotakala/LO4
git add . --dry-run
git add . 
git status
git commit -m "initial commit"
git status 
git push --set-upstream origin main
